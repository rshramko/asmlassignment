﻿using System.Collections;
using System.Collections.Generic;

namespace SequenceAnalysis.Test.ProblemSolvers
{
    public class ValidInputTestCases : IEnumerable<object[]>
    {
        private readonly List<object[]> _testCases = new List<object[]>
        {
            new object[] { "tEst", string.Empty },
            new object[] { "t", string.Empty },
            new object[] { "T", "T" },
            new object[] { "TEST", "ESTT" },
            new object[] { "This IS a STRING", "GIINRSST" },
            new object[] { "  This IS    a STRING ", "GIINRSST" }
        };

        public IEnumerator<object[]> GetEnumerator()
        {
            return _testCases.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
