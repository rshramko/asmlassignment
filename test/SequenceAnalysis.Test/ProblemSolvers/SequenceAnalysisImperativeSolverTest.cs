﻿using System;
using SequenceAnalysis.ProblemSolvers;
using Xunit;

namespace SequenceAnalysis.Test.ProblemSolvers
{
    public class SequenceAnalysisImperativeSolverTest
    {
        private readonly SequenceAnalysisImperativeSolver _target;

        public SequenceAnalysisImperativeSolverTest()
        {
            _target = new SequenceAnalysisImperativeSolver();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldThrowOnInvalidInput(string invalidInput)
        {
            Assert.Throws<ArgumentException>(() => _target.Solve(invalidInput));
        }

        [Theory]
        [ClassData(typeof(ValidInputTestCases))]
        public void ShouldSolveProblem(string input, string expectedResult)
        {
            string result = _target.Solve(input);

            Assert.Equal(expectedResult, result);
        }
    }
}
