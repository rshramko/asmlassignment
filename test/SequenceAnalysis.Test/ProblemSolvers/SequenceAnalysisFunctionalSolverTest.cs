﻿using System;
using SequenceAnalysis.ProblemSolvers;
using Xunit;

namespace SequenceAnalysis.Test.ProblemSolvers
{
    public class SequenceAnalysisFunctionalSolverTest
    {
        private readonly SequenceAnalysisFunctionalSolver _target;

        public SequenceAnalysisFunctionalSolverTest()
        {
            _target = new SequenceAnalysisFunctionalSolver();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ShouldThrowOnInvalidInput(string invalidInput)
        {
            Assert.Throws<ArgumentException>(() => _target.Solve(invalidInput));
        }

        [Theory]
        [ClassData(typeof(ValidInputTestCases))]
        public void ShouldSolveProblem(string input, string expectedResult)
        {
            string result = _target.Solve(input);

            Assert.Equal(expectedResult, result);
        }
    }
}
