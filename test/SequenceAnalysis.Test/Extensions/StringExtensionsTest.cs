﻿using System.Collections.Generic;
using SequenceAnalysis.Extensions;
using Xunit;

namespace SequenceAnalysis.Test.Extensions
{
    public class StringExtensionsTest
    {
        [Fact]
        public void SplitIntoTokensToleratesNullInput()
        {
            string input = null;

            IEnumerable<string> result = input.SplitIntoTokens();

            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void SplitIntoTokensSplitsStrings()
        {
            var input = "this is a test string";
            var expectedSequence = new[] { "this", "is", "a", "test", "string" };

            IEnumerable<string> result = input.SplitIntoTokens();

            Assert.Equal(expectedSequence, result);
        }

        [Fact]
        public void ConcatToleratesNullSequence()
        {
            char[] sequence = null;

            string result = sequence.Concat();

            Assert.Null(result);
        }

        [Fact]
        public void ConcatProcessesSequenceOfChars()
        {
            char[] sequence = { 't', 'e', 's', 't' };
            string expectedResult = "test";

            string result = sequence.Concat();

            Assert.Equal(expectedResult, result);
        }
    }
}
