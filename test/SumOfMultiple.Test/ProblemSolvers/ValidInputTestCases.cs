﻿using System.Collections;
using System.Collections.Generic;

namespace SumOfMultiple.Test.ProblemSolvers
{
    public class ValidInputTestCases : IEnumerable<object[]>
    {
        private readonly List<object[]> _testCases = new List<object[]>
        {
            new object[] { 1, 0L },
            new object[] { 3, 0L },
            new object[] { 5, 3L },
            new object[] { 6, 8L },
            new object[] { 51, 593L }
        };

        public IEnumerator<object[]> GetEnumerator()
        {
            return _testCases.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
