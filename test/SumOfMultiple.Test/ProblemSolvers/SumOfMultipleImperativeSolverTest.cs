﻿using System;
using SumOfMultiple.ProblemSolvers;
using Xunit;

namespace SumOfMultiple.Test.ProblemSolvers
{
    public class SumOfMultipleImperativeSolverTest
    {
        private readonly SumOfMultipleImperativeSolver _target;

        public SumOfMultipleImperativeSolverTest()
        {
            _target = new SumOfMultipleImperativeSolver();
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldThrowOnInvalidInput(int invalidInput)
        {
            Assert.Throws<ArgumentException>(() => _target.Solve(invalidInput));
        }

        [Theory]
        [ClassData(typeof(ValidInputTestCases))]
        public void ShouldSolveProblem(int input, long expectedResult)
        {
            long result = _target.Solve(input);

            Assert.Equal(expectedResult, result);
        }
    }
}
