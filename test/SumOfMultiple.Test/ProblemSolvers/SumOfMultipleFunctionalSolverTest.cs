﻿using System;
using SumOfMultiple.ProblemSolvers;
using Xunit;

namespace SumOfMultiple.Test.ProblemSolvers
{
    public class SumOfMultipleFunctionalSolverTest
    {
        private readonly SumOfMultipleFunctionalSolver _target;

        public SumOfMultipleFunctionalSolverTest()
        {
            _target = new SumOfMultipleFunctionalSolver();
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void ShouldThrowOnInvalidInput(int invalidInput)
        {
            Assert.Throws<ArgumentException>(() => _target.Solve(invalidInput));
        }

        [Theory]
        [ClassData(typeof(ValidInputTestCases))]
        public void ShouldSolveProblem(int input, long expectedResult)
        {
            long result = _target.Solve(input);

            Assert.Equal(expectedResult, result);
        }
    }
}
