﻿using Common.OSInteraction;
using Common.UserInteraction;
using Moq;
using Xunit;
using TerminalUI.UserCommands;

namespace TerminalUI.Test.UserCommands
{
    public class ExitCommandTest
    {
        private readonly Mock<IOSInteractionProvider> _osInteractionProviderMock;
        private readonly Mock<IUserInteractionProvider> _userInteractionProviderMock;

        private readonly ExitCommand _target;

        public ExitCommandTest()
        {
            _osInteractionProviderMock = new Mock<IOSInteractionProvider>();
            _userInteractionProviderMock = new Mock<IUserInteractionProvider>();

            _target = new ExitCommand(
                _osInteractionProviderMock.Object,
                _userInteractionProviderMock.Object);
        }

        [Fact]
        public void ShouldExitAppWithMessage()
        {
            _target.Execute();

            _userInteractionProviderMock
                .Verify(m => m.ShowMessage("Bye......."), Times.Once);
            _osInteractionProviderMock
                .Verify(m => m.ExitApplication(), Times.Once);
        }
    }
}
