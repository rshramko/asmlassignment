﻿using Common.Solvers;
using Common.UserInteraction;
using Moq;
using TerminalUI.UserCommands;
using Xunit;

namespace TerminalUI.Test.UserCommands
{
    public class SumOfMultipleCommandTest
    {
        private readonly Mock<IUserInteractionProvider> _interactionProviderMock;
        private readonly Mock<ISumOfMultipleSolver> _solverMock;

        private readonly SumOfMultipleCommand _target;

        public SumOfMultipleCommandTest()
        {
            _interactionProviderMock = new Mock<IUserInteractionProvider>();
            _solverMock = new Mock<ISumOfMultipleSolver>();

            _target = new SumOfMultipleCommand(
                _solverMock.Object,
                _interactionProviderMock.Object);
        }

        [Fact]
        public void ShouldRunSolverWithValidInput()
        {
            int userInput = 1234;
            const long solverResult = 99999L;

            _interactionProviderMock.Setup(
                m => m.RequestNumber(It.IsAny<string>(), out userInput))
                .Returns(true);

            _solverMock.Setup(m => m.Solve(userInput)).Returns(solverResult);

            _target.Execute();

            _solverMock.Verify(m => m.Solve(userInput), Times.Once);

            // should show result to user
            _interactionProviderMock.Verify(
                m => m.ShowMessage(
                    It.Is<string>(s => s.Contains(solverResult.ToString()))),
                Times.Once);
        }

        [Fact]
        public void ShouldNotRunSolverOnInvalidInput()
        {
            int userInput = -1;

            _interactionProviderMock.Setup(
                m => m.RequestNumber(It.IsAny<string>(), out userInput))
                .Returns(false);

            _target.Execute();

            _solverMock.Verify(m => m.Solve(It.IsAny<int>()), Times.Never);

            _interactionProviderMock.Verify(m => m.ShowMessage("Invalid input"), Times.Once);
        }
    }
}
