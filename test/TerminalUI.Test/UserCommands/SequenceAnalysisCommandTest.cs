﻿using Common.Solvers;
using Common.UserInteraction;
using Moq;
using TerminalUI.UserCommands;
using Xunit;

namespace TerminalUI.Test.UserCommands
{
    public class SequenceAnalysisCommandTest
    {
        private readonly Mock<IUserInteractionProvider> _interactionProviderMock;
        private readonly Mock<ISequenceAnalysisSolver> _solverMock;

        private readonly SequenceAnalysisCommand _target;

        public SequenceAnalysisCommandTest()
        {
            _interactionProviderMock = new Mock<IUserInteractionProvider>();
            _solverMock = new Mock<ISequenceAnalysisSolver>();

            _target = new SequenceAnalysisCommand(
                _solverMock.Object,
                _interactionProviderMock.Object);
        }

        [Fact]
        public void ShouldRunSolverWithValidInput()
        {
            string userInput = "test";
            const string solverResult = "result";

            _interactionProviderMock.Setup(
                m => m.RequestNonEmptyString(It.IsAny<string>(), out userInput))
                .Returns(true);

            _solverMock.Setup(m => m.Solve(userInput)).Returns(solverResult);

            _target.Execute();

            _solverMock.Verify(m => m.Solve(userInput), Times.Once);

            // should show result to user
            _interactionProviderMock.Verify(
                m => m.ShowMessage(
                    It.Is<string>(s => s.Contains(solverResult))),
                Times.Once);
        }

        [Fact]
        public void ShouldNotRunSolverOnInvalidInput()
        {
            string userInput = string.Empty;

            _interactionProviderMock.Setup(
                m => m.RequestNonEmptyString(It.IsAny<string>(), out userInput))
                .Returns(false);

            _target.Execute();

            _solverMock.Verify(m => m.Solve(It.IsAny<string>()), Times.Never);

            _interactionProviderMock.Verify(m => m.ShowMessage("Invalid input"), Times.Once);
        }
    }
}
