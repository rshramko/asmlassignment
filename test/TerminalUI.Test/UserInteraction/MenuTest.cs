﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.UserInteraction;
using Moq;
using TerminalUI.UserCommands;
using TerminalUI.UserInteraction;
using Xunit;

namespace TerminalUI.Test.UserInteraction
{
    public class MenuTest
    {
        private readonly List<Mock<IUserCommand>> _commandMocks;
        private readonly Mock<IUserInteractionProvider> _interactionProviderMock;

        private readonly Menu _target;

        public MenuTest()
        {
            _commandMocks = MakeCommands();
            _interactionProviderMock = new Mock<IUserInteractionProvider>();

            // setup default behavior for interaction provider
            int selectedMenuOption = 1;
            _interactionProviderMock.Setup(m => m.RequestNumber(It.IsAny<string>(), out selectedMenuOption))
                .Returns(true);

            _target = new Menu(
                _commandMocks.Select(x => x.Object).ToList(),
                _interactionProviderMock.Object);
        }

        [Fact]
        public void ShouldShowAvailableOptions()
        {
            // here we rely on default user input setup in ctor
            _target.Run();

            _interactionProviderMock.Verify(m => m.ShowMessage("Available options:"));
            _interactionProviderMock.Verify(m => m.ShowMessage("1 - Mock1"));
            _interactionProviderMock.Verify(m => m.ShowMessage("2 - Mock2"));
        }

        [Fact]
        public void ShouldExecuteRequestedCommand()
        {
            int selectedMenuOption = 2;
            int expectedCommandToRunIndex = selectedMenuOption - 1;

            _interactionProviderMock.Setup(m => m.RequestNumber(It.IsAny<string>(), out selectedMenuOption))
                .Returns(true);

            _target.Run();

            _commandMocks[expectedCommandToRunIndex].Verify(
                m => m.Execute(), Times.Once);
        }

        [Fact]
        public void ShouldHandleInvalidInput()
        {
            int selectedMenuOption = -1;

            _interactionProviderMock.Setup(m => m.RequestNumber(It.IsAny<string>(), out selectedMenuOption))
                .Returns(true);

            _target.Run();

            _commandMocks.ForEach(c => c.Verify(m => m.Execute(), Times.Never));
            _interactionProviderMock.Verify(m => m.ShowMessage("Wrong choice\n"));
        }

        [Fact]
        public void ShouldHandleExceptions()
        {
            _commandMocks[0].Setup(m => m.Execute())
                .Throws(new Exception("Test Error"));

            // here we rely on default user input setup in ctor
            _target.Run();

            _interactionProviderMock.Verify(m => m.ShowMessage("Test Error"));
        }

        private static List<Mock<IUserCommand>> MakeCommands()
        {
            var mock1 = new Mock<IUserCommand>();
            mock1.SetupGet(m => m.Description).Returns("Mock1");

            var mock2 = new Mock<IUserCommand>();
            mock2.SetupGet(m => m.Description).Returns("Mock2");

            return new List<Mock<IUserCommand>>
            {
                mock1,
                mock2
            };
        }
    }
}
