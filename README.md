# ASML Test Assignment

### How to run the code

The solution is built using .NET Framework 4.6 as a target framework, so please use VisualStudio 2015/2017, or JetBrains Rider for opening.

Running the solution from JetBrains Rider on OS X or Linux platforms via built-in terminal, 
has some issues with System.Console.ReadLine(), so please build the solution,
and then execute Runner.exe directly from an external terminal. 

### How to run unit test

The solution uses XUnit as a test framework, with the "xunit.runner.visualstudio" as a NuGet dependency for test projects,
so it should be able to run all the test suites directly from Visual Studio.

In case any issues please check the [Running tests with Visual Studio](https://xunit.github.io/docs/getting-started-desktop.html#run-tests-visualstudio) 
XUnit documentation.
