﻿namespace Common.Solvers
{
    /// <summary>
    /// "Sequence Analysis" problem solver
    /// </summary>
    public interface ISequenceAnalysisSolver
    {
        /// <summary>
        /// Finds the uppercased words in the input string,
        /// and orders all characters in those words alphabetically
        /// </summary>
        /// <param name="input">The string to be processed</param>
        /// <returns>A string comprised of ordered characters found</returns>
        string Solve(string input);
    }
}
