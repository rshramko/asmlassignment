﻿namespace Common.Solvers
{
    /// <summary>
    /// "Sum of Multiple" problem solver
    /// </summary>
    public interface ISumOfMultipleSolver
    {
        /// <summary>
        /// Finds the sum of all natural numbers that are
        /// a multiple of 3 or 5 below a limit provided
        /// </summary>
        /// <param name="limit">The limit to restrict the summing</param>
        /// <returns>The sum of numbers</returns>
        long Solve(int limit);
    }
}
