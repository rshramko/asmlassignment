﻿namespace Common.OSInteraction
{
    /// <summary>
    /// An abstraction to interact with operating system
    /// </summary>
    public interface IOSInteractionProvider
    {
        /// <summary>
        /// Quit application with successful exit code
        /// </summary>
        void ExitApplication();
    }
}
