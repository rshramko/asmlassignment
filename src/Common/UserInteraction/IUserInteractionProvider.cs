﻿namespace Common.UserInteraction
{
    /// <summary>
    /// An abstraction to interact with a user
    /// </summary>
    public interface IUserInteractionProvider
    {
        /// <summary>
        /// Requests a number from a user
        /// </summary>
        /// <param name="prompt">A prompt to be shown</param>
        /// <param name="value">A value provided by user in case of correct input</param>
        /// <returns>Returns <c>true</c>, if the input was correct, otherwise - <c>false</c></returns>
        bool RequestNumber(string prompt, out int value);

        /// <summary>
        /// Requests a string from a user
        /// </summary>
        /// <param name="prompt">A prompt to be shown</param>
        /// <param name="value">A value provided by user in case of correct input</param>
        /// <returns>Returns <c>true</c>, if the input was correct, otherwise - <c>false</c></returns>
        bool RequestNonEmptyString(string prompt, out string value);

        /// <summary>
        /// Prints a given message
        /// </summary>
        /// <param name="message">A message to be shown</param>
        void ShowMessage(string message);
    }
}
