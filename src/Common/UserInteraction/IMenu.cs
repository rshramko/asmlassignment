﻿namespace Common.UserInteraction
{
    /// <summary>
    /// Represents a user menu
    /// </summary>
    public interface IMenu
    {
        /// <summary>
        /// Runs main menu
        /// </summary>
        void Run();
    }
}
