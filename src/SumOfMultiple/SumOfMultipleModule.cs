﻿using Autofac;
using SumOfMultiple.ProblemSolvers;

namespace SumOfMultiple
{
    public class SumOfMultipleModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<SumOfMultipleFunctionalSolver>().AsImplementedInterfaces();
        }
    }
}
