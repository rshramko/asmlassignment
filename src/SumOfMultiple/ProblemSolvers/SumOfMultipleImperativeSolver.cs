﻿using System;
using Common.Solvers;

namespace SumOfMultiple.ProblemSolvers
{
    /// <summary>
    /// Implementation of "Sum Of Multiple" problem solver using
    /// imperative style approach.
    /// </summary>
    internal class SumOfMultipleImperativeSolver : ISumOfMultipleSolver
    {
        public long Solve(int limit)
        {
            if (limit < 1)
            {
                throw new ArgumentException(nameof(limit));
            }

            long result = 0;

            for (int i = 0; i < limit; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    result += i;
                }
            }

            return result;
        }
    }
}
