﻿using System;
using System.Linq;
using Common.Solvers;

namespace SumOfMultiple.ProblemSolvers
{
    /// <summary>
    /// Implementation of "Sum Of Multiple" problem solver using
    /// functional style approach.
    /// </summary>
    internal class SumOfMultipleFunctionalSolver : ISumOfMultipleSolver
    {
        public long Solve(int limit)
        {
            if (limit < 1)
            {
                throw new ArgumentException(nameof(limit));
            }

            return Enumerable.Range(1, limit - 1).Where(IsMultiple).Sum();
        }

        private static bool IsMultiple(int number)
        {
            return number % 5 == 0 || number % 3 == 0;
        }
    }
}
