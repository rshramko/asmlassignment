﻿using System;
using System.Linq;
using Common.Solvers;
using SequenceAnalysis.Extensions;

namespace SequenceAnalysis.ProblemSolvers
{
    /// <summary>
    /// Implementation of "Sequence Analysis" problem solver using
    /// functional style approach.
    /// </summary>
    internal class SequenceAnalysisFunctionalSolver : ISequenceAnalysisSolver
    {
        public string Solve(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentException(input);
            }

            return input
                    .SplitIntoTokens()
                    .Where(IsUppercasedToken)
                    .SelectMany(ch => ch)
                    .OrderBy(ch => ch)
                    .Concat();
        }

        private static bool IsUppercasedToken(string token)
        {
            // Potentially could also go into StringExtensions.
            // I've decided to keep it here just to use as a group method
            // in the Where() filter
            return token.All(Char.IsUpper);
        }
    }
}
