﻿using System;
using System.Collections.Generic;
using Common.Solvers;

namespace SequenceAnalysis.ProblemSolvers
{
    /// <summary>
    /// Implementation of "Sequence Analysis" problem solver using
    /// imperative style approach.
    /// </summary>
    internal class SequenceAnalysisImperativeSolver : ISequenceAnalysisSolver
    {
        public string Solve(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentException(input);
            }

            List<char> resultChars = SelectCharsFromUpperCasedWords(input);

            resultChars.Sort();

            return new string(resultChars.ToArray());
        }

        private static List<char> SelectCharsFromUpperCasedWords(string input)
        {
            /*
             * Please note, that the logic in this method is not an idiomatic C# code,
             * and should be replaced by using string.Split() and Linq methods
             * in the vast majority of cases.
             *
             * The reason of using it here, just to have something completely opposite to the
             * logic of the functional solver.
             */
            var selectedChars = new List<char>();

            int wordStartPos = 0;
            int lastInputPos = input.Length - 1;
            bool isUpperCasedWord = true;

            for (int i = 0; i <= lastInputPos; i++)
            {
                char currentChar = input[i];
                bool isLastCharacter = i == lastInputPos;

                if (ShouldResetUpperCaseFlag(currentChar))
                {
                    isUpperCasedWord = false;
                }

                if (FoundEndOfToken(currentChar, isLastCharacter))
                {
                    int wordEndPos = CalculateWordEndPosition(i, currentChar, isLastCharacter);
                    if (IsRealUpperCasedWord(isUpperCasedWord, wordStartPos, wordEndPos))
                    {
                        CopyWordIntoResultList(input, wordStartPos, wordEndPos, selectedChars);
                    }
                    isUpperCasedWord = true;
                    wordStartPos = i + 1;
                }
            }

            return selectedChars;
        }

        private static void CopyWordIntoResultList(string input, int startPos, int endPos, List<char> result)
        {
            for (int i = 0; i < endPos - startPos; i++)
            {
                result.Add(input[i + startPos]);
            }
        }

        private static bool ShouldResetUpperCaseFlag(char currentChar)
        {
            return char.IsLower(currentChar);
        }

        private static bool FoundEndOfToken(char currentCharacter, bool isLastCharacter)
        {
            return char.IsWhiteSpace(currentCharacter) || isLastCharacter;
        }

        private static int CalculateWordEndPosition(
            int currentPosition, char currentCharacter, bool isLastCharacter)
        {
            if (!char.IsWhiteSpace(currentCharacter) && isLastCharacter)
            {
                return currentPosition + 1;
            }

            return currentPosition;
        }

        private static bool IsRealUpperCasedWord(bool uppercaseWordFlag, int wordStartPos, int wordEndPos)
        {
            return uppercaseWordFlag && wordStartPos != wordEndPos;
        }
    }
}
