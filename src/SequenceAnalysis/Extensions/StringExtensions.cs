﻿using System;
using System.Collections.Generic;

namespace SequenceAnalysis.Extensions
{
    /// <summary>
    /// Provides extension methods for the <see cref="System.String"/> class.
    /// </summary>
    internal static class StringExtensions
    {
        /// <summary>
        /// Provides a wrapper around the string.Split() method,
        /// which returns a sequence of tokens from a given input
        /// </summary>
        /// <param name="input">The string to be processed</param>
        /// <param name="separator">Separator</param>
        /// <returns>A sequence of tokens</returns>
        public static IEnumerable<string> SplitIntoTokens(this string input, char separator = ' ')
        {
            if (!string.IsNullOrEmpty(input))
            {
                var tokens = input.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var token in tokens)
                {
                    yield return token;
                }
            }
        }

        /// <summary>
        /// Concatenates a given sequence of characters into a single string
        /// </summary>
        /// <param name="sequence">A sequence of characters to be concatenated</param>
        /// <returns>A result string if sequence is not <c>null</c>, otherwise - <c>null</c></returns>
        public static string Concat(this IEnumerable<char> sequence)
        {
            // Well, with the only use-case of this method in the current solution it's not possible
            // to have a null sequence as an input, but this is an extension method
            // which potentially can be reused in different scenarios,
            // so it's better to add the safety check for null.
            if (sequence != null)
            {
                return string.Join("", sequence);
            }

            return null;
        }
    }
}
