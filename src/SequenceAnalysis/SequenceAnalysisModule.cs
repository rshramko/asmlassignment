﻿using Autofac;
using SequenceAnalysis.ProblemSolvers;

namespace SequenceAnalysis
{
    public class SequenceAnalysisModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<SequenceAnalysisFunctionalSolver>().AsImplementedInterfaces();
        }
    }
}
