﻿using Autofac;
using Common.UserInteraction;
using TerminalUI.UserCommands;
using TerminalUI.UserInteraction;

namespace TerminalUI
{
    public class TerminalUIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            // Please note that order of registration influences user menu items
            builder.RegisterType<SequenceAnalysisCommand>().As<IUserCommand>();
            builder.RegisterType<SumOfMultipleCommand>().As<IUserCommand>();
            builder.RegisterType<ExitCommand>().As<IUserCommand>();

            builder.RegisterType<Menu>().As<IMenu>();
        }
    }
}
