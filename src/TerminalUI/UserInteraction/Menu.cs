﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.UserInteraction;
using TerminalUI.UserCommands;

namespace TerminalUI.UserInteraction
{
    internal class Menu : IMenu
    {
        private static readonly IUserCommand NoOp = new NoOpCommand();

        private readonly List<IUserCommand> _userCommands;
        private readonly IUserInteractionProvider _interactionProvider;

        public Menu(
            IEnumerable<IUserCommand> userCommands,
            IUserInteractionProvider interactionProvider)
        {
            if (userCommands == null)
            {
                throw new ArgumentNullException(nameof(userCommands));
            }

            _userCommands = userCommands.ToList();

            _interactionProvider = interactionProvider ?? throw new ArgumentNullException(nameof(interactionProvider));
        }

        public void Run()
        {
            ShowAvailableCommands();
            _interactionProvider.ShowMessage(string.Empty);

            IUserCommand command = RequestCommandToExecute();

            ExecuteCommand(command);
            _interactionProvider.ShowMessage(string.Empty);
        }

        private void ShowAvailableCommands()
        {
            _interactionProvider.ShowMessage("Available options:");

            for (int i = 0; i < _userCommands.Count; i++)
            {
                _interactionProvider.ShowMessage($"{i + 1} - {_userCommands[i].Description}");
            }
        }

        private IUserCommand RequestCommandToExecute()
        {
            int choice;

            if (!_interactionProvider.RequestNumber("Please, make your choice", out choice) ||
                choice < 1 ||
                choice > _userCommands.Count)
            {
                _interactionProvider.ShowMessage("Wrong choice\n");
                return NoOp;
            }

            return _userCommands[choice - 1];
        }

        private void ExecuteCommand(IUserCommand command)
        {
            try
            {
                command.Execute();
            }
            catch (Exception ex)
            {
                _interactionProvider.ShowMessage(ex.Message);
            }
        }

        /// <summary>
        /// Represents NoOp command which is used in case of wrong user choice.
        /// It's only supposed to be used by the Menu, so it' made private on intention.
        ///
        /// Alternatively might be implemented as a regular Command class,
        /// which is used directly (without registering in the IOC container).
        /// </summary>
        private class NoOpCommand : IUserCommand
        {
            public string Description => "NoOp";

            public void Execute()
            {
            }
        }
    }
}
