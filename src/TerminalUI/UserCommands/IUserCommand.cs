﻿namespace TerminalUI.UserCommands
{
    /// <summary>
    /// Defines a command which should be executed upon a user menu item selection
    /// </summary>
    internal interface IUserCommand
    {
        /// <summary>
        /// Returns a description of the command which will be
        /// a part of prompt shown to the user
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Execute command
        /// </summary>
        void Execute();
    }
}
