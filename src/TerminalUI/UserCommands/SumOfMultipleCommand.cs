﻿using System;
using Common.Solvers;
using Common.UserInteraction;

namespace TerminalUI.UserCommands
{
    internal class SumOfMultipleCommand : IUserCommand
    {
        private const string CommandDescription = "Calculate Sum of Multiple";
        private const string Prompt = "Please, provide a positive number";
        private const string InvalidInput = "Invalid input";

        private readonly ISumOfMultipleSolver _solver;
        private readonly IUserInteractionProvider _interactionProvider;

        public SumOfMultipleCommand(
            ISumOfMultipleSolver solver,
            IUserInteractionProvider interactionProvider)
        {
            _solver = solver ?? throw new ArgumentNullException(nameof(solver));
            _interactionProvider = interactionProvider ?? throw new ArgumentNullException(nameof(interactionProvider));
        }

        public string Description => CommandDescription;

        public void Execute()
        {
            int number;

            if (_interactionProvider.RequestNumber(Prompt, out number) && number > 0)
            {
                long result = _solver.Solve(number);

                _interactionProvider.ShowMessage($"Result: {result}");
            }
            else
            {
                _interactionProvider.ShowMessage(InvalidInput);
            }
        }
    }
}
