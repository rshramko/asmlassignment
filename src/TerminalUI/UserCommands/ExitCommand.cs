﻿using System;
using System.Threading;
using Common.UserInteraction;
using Common.OSInteraction;

namespace TerminalUI.UserCommands
{
    internal class ExitCommand : IUserCommand
    {
        private const string CommandDescription = "Quit";
        private const string ByeMessage = "Bye.......";

        private readonly IOSInteractionProvider _osInteractionProvider;
        private readonly IUserInteractionProvider _userInteractionProvider;

        public ExitCommand(
            IOSInteractionProvider osInteractionProvider,
            IUserInteractionProvider userInteractionProvider)
        {
            _osInteractionProvider = osInteractionProvider ?? throw new ArgumentNullException(nameof(osInteractionProvider));
            _userInteractionProvider = userInteractionProvider ?? throw new ArgumentNullException(nameof(userInteractionProvider));
        }

        public string Description => CommandDescription;

        public void Execute()
        {
            _userInteractionProvider.ShowMessage(ByeMessage);

            // just sleep for a tiny bit to allow user to see bye message
            Thread.Sleep(TimeSpan.FromMilliseconds(500));

            _osInteractionProvider.ExitApplication();
        }
    }
}
