﻿using System;
using Common.Solvers;
using Common.UserInteraction;

namespace TerminalUI.UserCommands
{
    internal class SequenceAnalysisCommand : IUserCommand
    {
        private const string CommandDescription = "Run Sequence Analysis";
        private const string Prompt = "Please, provide a string";
        private const string InvalidInput = "Invalid input";

        private readonly ISequenceAnalysisSolver _solver;
        private readonly IUserInteractionProvider _interactionProvider;

        public SequenceAnalysisCommand(
            ISequenceAnalysisSolver solver,
            IUserInteractionProvider interactionProvider)
        {
            _solver = solver ?? throw new ArgumentNullException(nameof(solver));
            _interactionProvider = interactionProvider ?? throw new ArgumentNullException(nameof(interactionProvider));
        }

        public string Description => CommandDescription;

        public void Execute()
        {
            string stringToBeProcessed;

            if (_interactionProvider.RequestNonEmptyString(Prompt, out stringToBeProcessed))
            {
                string result = _solver.Solve(stringToBeProcessed);

                _interactionProvider.ShowMessage($"Result: {result}");
            }
            else
            {
                _interactionProvider.ShowMessage(InvalidInput);
            }
        }
    }
}
