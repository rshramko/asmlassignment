﻿using Autofac;
using Common.UserInteraction;
using SequenceAnalysis;
using SumOfMultiple;
using TerminalUI;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            IMenu menu = BuildContainerAndGetMenu();

            while (true)
            {
                menu.Run();
            }
        }

        private static IMenu BuildContainerAndGetMenu()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new RunnerModule());
            builder.RegisterModule(new SequenceAnalysisModule());
            builder.RegisterModule(new SumOfMultipleModule());
            builder.RegisterModule(new TerminalUIModule());

            var container = builder.Build();

            return container.Resolve<IMenu>();
        }
    }
}
