﻿using Autofac;
using Common.OSInteraction;
using Common.UserInteraction;
using Runner.InteractionProviders;

namespace Runner
{
    public class RunnerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<OSInteractionProvider>().As<IOSInteractionProvider>();
            builder.RegisterType<UserInteractionProvider>().As<IUserInteractionProvider>();
        }
    }
}
