﻿using System;
using Common.OSInteraction;

namespace Runner.InteractionProviders
{
    internal class OSInteractionProvider : IOSInteractionProvider
    {
        public void ExitApplication()
        {
            Environment.Exit(0);
        }
    }
}
