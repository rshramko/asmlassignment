﻿using System;
using Common.UserInteraction;

namespace Runner.InteractionProviders
{
    internal class UserInteractionProvider : IUserInteractionProvider
    {
        public bool RequestNumber(string prompt, out int value)
        {
            string input;
            if (RequestNonEmptyString(prompt, out input))
            {
                return int.TryParse(input, out value);
            }

            value = default(int);
            return false;
        }

        public bool RequestNonEmptyString(string prompt, out string value)
        {
            Console.Write("{0}: ", prompt);
            value = Console.ReadLine()?.Trim();

            return !string.IsNullOrEmpty(value);
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
